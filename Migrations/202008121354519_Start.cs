﻿namespace Demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Danias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nazwa = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Etaps",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nrCzynnosci = c.Int(nullable: false),
                        liczbaPracownikow = c.Int(nullable: false),
                        typPracownikow = c.Int(nullable: false),
                        czasWykonania = c.Int(nullable: false),
                        nastepnikCzynnosci = c.Int(nullable: false),
                        czasZwloki = c.Int(nullable: false),
                        stanowisko = c.Int(nullable: false),
                        Dania_Id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Danias", t => t.Dania_Id)
                .Index(t => t.Dania_Id);
            
            CreateTable(
                "dbo.Pracowniks",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        R = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Stanowiskoes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        R = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Etaps", "Dania_Id", "dbo.Danias");
            DropIndex("dbo.Etaps", new[] { "Dania_Id" });
            DropTable("dbo.Stanowiskoes");
            DropTable("dbo.Pracowniks");
            DropTable("dbo.Etaps");
            DropTable("dbo.Danias");
        }
    }
}
