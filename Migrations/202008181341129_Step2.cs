﻿namespace Demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Step2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pracowniks", "typ", c => c.Int(nullable: false));
            AddColumn("dbo.Pracowniks", "human", c => c.Boolean(nullable: false));
            AddColumn("dbo.Stanowiskoes", "typ", c => c.Int(nullable: false));
            AddColumn("dbo.Stanowiskoes", "human", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stanowiskoes", "human");
            DropColumn("dbo.Stanowiskoes", "typ");
            DropColumn("dbo.Pracowniks", "human");
            DropColumn("dbo.Pracowniks", "typ");
        }
    }
}
