﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Xceed.Wpf.Toolkit;
using Demo.Objects;
using System.Reflection;

namespace Demo
{

    public partial class MainWindow : Window
    {
        public delegate int Funkcja(List<Pracownik> pracLS, List<Stanowisko> stanLS, List<Dania> dania,bool show);
        public delegate void Funkcja1(ref Dania [] dania, Random rand);
        private Dane.Model db = new Dane.Model();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Pracownik> pracLS = new List<Pracownik>();
                List<Stanowisko> stanLS = new List<Stanowisko>();
                List<Dania> dania = new List<Dania>();
                List<Zasoby> zasobyLS = new List<Zasoby>();
                CreateSupply(ref stanLS, GetNumberOfSuplies(grid2), false);
                CreateSupply(ref pracLS, GetNumberOfSuplies(grid3), true);
                Algorytm algorytm = new Algorytm(GetNumberOfSuplies(gridParam));
                foreach (var x in db.Danias)
                {
                    foreach (var t in db.Etaps)
                    {

                    }
                }
                
                dania = db.Danias.ToList();
                var helpGetDishes = grid1.Children.OfType<IntegerUpDown>().ToList();
                for (int i = dania.Count; i > helpGetDishes[0].Value; i--)
                {
                    dania.RemoveAt(dania.Count - 1);
                }
                var butHam = gridHam.Children.OfType<RadioButton>().FirstOrDefault(r => (bool)r.IsChecked);
                var butEdit = gridMod.Children.OfType<RadioButton>().FirstOrDefault(r => (bool)r.IsChecked);
                Funkcja fun = algorytm.alg1;
                Funkcja1 fun1 = algorytm.swap;
                switch (butHam.Tag)
                {
                    case "alg1":
                        fun = algorytm.alg1;
                        break;
                    case "alg2":
                        fun = algorytm.alg2;
                        break;
                    case "alg3":
                        fun = algorytm.alg3;
                        break;
                }
                switch (butEdit.Tag)
                {
                    case "swap":
                        fun1 = algorytm.swap;
                        break;
                    case "swap2":
                        fun1 = algorytm.swap2;
                        break;
                    case "insert":
                        fun1 = algorytm.insert;
                        break;
                }
                algorytm.wyzarzanie(pracLS, stanLS, dania, fun, fun1);
                
            }
            catch (ArgumentOutOfRangeException) { System.Windows.Forms.MessageBox.Show("Wszystkie wartośći muszą być większe od zera"); }
            catch (IndexOutOfRangeException) { System.Windows.Forms.MessageBox.Show("Wszystkie wartośći muszą być większe od zera"); }
            catch (Exception) { System.Windows.Forms.MessageBox.Show("Błąd programu"); }
        }

        public void CreateSupply<T>(ref List<T> cos, int [] ilosc, bool hum)
        {
            int i = 0,j=0, obiekty=ilosc.Sum();
            while (i < obiekty)
            {
                if (ilosc[j] != 0) 
                {
                    cos.Add((T)Activator.CreateInstance(typeof(T), j+1, hum));
                    i++;
                    ilosc[j]--;
                }
                else
                {
                    j++;
                }
            }
        }
        public int [] GetNumberOfSuplies(Grid grid)
        {
            int [] result = new int [grid.Children.Count];
            int i = 0;
            foreach(IntegerUpDown x in grid.Children.OfType<IntegerUpDown>())
            {
                result[i] += (int) x.Value;
                i++;
            }
            return result;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (db.Etaps.ToList() == null)
            {
                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 2, typPracownikow = 1, czasWykonania = 30, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 2, typPracownikow = 3, czasWykonania = 5, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 6, czasWykonania = 15, nastepnikCzynnosci = 2, czasZwloki = 15, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 2, typPracownikow = 1, czasWykonania = 30, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 2, typPracownikow = 3, czasWykonania = 60, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 6, czasWykonania = 15, nastepnikCzynnosci = 2, czasZwloki = 15, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 1, typPracownikow = 1, czasWykonania = 45, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 1, typPracownikow = 5, czasWykonania = 45, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 5, czasWykonania = 5, nastepnikCzynnosci = 2, czasZwloki = 5, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 3, typPracownikow = 1, czasWykonania = 45, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 1, typPracownikow = 6, czasWykonania = 90, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 6, czasWykonania = 5, nastepnikCzynnosci = 2, czasZwloki = 5, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 2, typPracownikow = 1, czasWykonania = 45, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 2, typPracownikow = 2, czasWykonania = 90, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 6, czasWykonania = 10, nastepnikCzynnosci = 2, czasZwloki = 5, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 2, typPracownikow = 1, czasWykonania = 50, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 2, typPracownikow = 2, czasWykonania = 120, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 6, czasWykonania = 15, nastepnikCzynnosci = 2, czasZwloki = 5, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 1, typPracownikow = 3, czasWykonania = 5, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 1, typPracownikow = 3, czasWykonania = 45, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 3, czasWykonania = 5, nastepnikCzynnosci = 2, czasZwloki = 15, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 2, typPracownikow = 1, czasWykonania = 90, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 1, typPracownikow = 3, czasWykonania = 30, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 3, czasWykonania = 5, nastepnikCzynnosci = 2, czasZwloki = 15, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 2, typPracownikow = 1, czasWykonania = 45, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 2, typPracownikow = 2, czasWykonania = 60, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 6, czasWykonania = 10, nastepnikCzynnosci = 2, czasZwloki = 15, stanowisko = 2 });

                db.Etaps.Add(new Etap { nrCzynnosci = 1, liczbaPracownikow = 1, typPracownikow = 1, czasWykonania = 30, nastepnikCzynnosci = 0, czasZwloki = 100000, stanowisko = 1 });
                db.Etaps.Add(new Etap { nrCzynnosci = 2, liczbaPracownikow = 1, typPracownikow = 5, czasWykonania = 60, nastepnikCzynnosci = 1, czasZwloki = 120, stanowisko = 2 });
                db.Etaps.Add(new Etap { nrCzynnosci = 3, liczbaPracownikow = 1, typPracownikow = 5, czasWykonania = 5, nastepnikCzynnosci = 2, czasZwloki = 15, stanowisko = 2 });
                db.SaveChanges();
            }
            if (db.Danias.ToList() == null)
            {
                List<Etap> etap = db.Etaps.ToList();
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[0], etap[1], etap[2] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[3], etap[4], etap[5] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[6], etap[7], etap[8] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[9], etap[10], etap[11] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[12], etap[13], etap[14] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[15], etap[16], etap[17] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[18], etap[19], etap[20] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[21], etap[22], etap[23] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[24], etap[25], etap[26] } });
                db.Danias.Add(new Dania { Etapy = new List<Etap>() { etap[27], etap[28], etap[29] } });
                db.SaveChanges();
            }
        }
    }
}
