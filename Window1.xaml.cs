﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms.Integration;
using System.Drawing.Design;

namespace Demo
{
    public partial class Window1 : Window
    {
        static int nrOpen = 0;
        public Window1(int columns, int [,] tab,int[,] A)
        {
            InitializeComponent();
            DataGridView dataGrid = new DataGridView();
            WindowsFormsHost host = new WindowsFormsHost();
            host.Child = dataGrid;
            this.grid1.Children.Add(host);
            for (int k = 0; k < columns+1; k++)
            {
                DataGridViewColumn newCol = new DataGridViewColumn(); 
                DataGridViewCell cell = new DataGridViewTextBoxCell();
                newCol.CellTemplate = cell;

                newCol.HeaderText = k.ToString();
                newCol.Name =k.ToString();
                newCol.Visible = true;
                newCol.Width = 40;

                dataGrid.Columns.Add(newCol);
            }
            int max = tab.Cast<int>().Max();
            for (int k = 0; k < max/5; k++)
            {
                dataGrid.Rows.Add(1);
            }
            int cos = dataGrid.Rows.Count;
            fillGrid(tab, A,dataGrid);
            nrOpen++;
        }

        public void fillGrid(int[,] tab, int[,] A, DataGridView grid)
        {
            int pocz = 0, kon = 0,kol=0;
            for(int k = 0; k < tab.GetLength(0); k++)
            {
                for(int j = 0; j < A.GetLength(1); j++)
                {
                    if (A[k, j] != 0)
                    {
                        pocz = tab[k, 0]/5;
                        kon = tab[k, 1]/5;
                        if (A[k, j] > 31)
                            kol = A[k, j] - (nrOpen* (tab.GetLength(0)+1));
                        else
                            kol = A[k, j];
                        grid.Rows[pocz].Cells[kol].Style.BackColor = System.Drawing.Color.Green;
                        grid.Rows[pocz].Cells[kol].Value = k;
                        for (int i = pocz+1; i < kon; i++)
                        {
                            grid.Rows[i].Cells[kol].Style.BackColor = System.Drawing.Color.Gray;
                        }
                        grid.Rows[kon - 1].Cells[kol].Style.BackColor = System.Drawing.Color.Red;
                        grid.Rows[kon - 1].Cells[kol].Value = k;
                    }
                }
                
                
            }
        }

    }
}
