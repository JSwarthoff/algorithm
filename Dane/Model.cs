﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Demo.Dane
{
    class Model:DbContext
    {
        public DbSet<Objects.Dania> Danias { get; set; }
        public DbSet<Objects.Pracownik> Pracowniks { get; set; }
        public DbSet<Objects.Stanowisko> Stanowiskos { get; set; }
        public DbSet<Objects.Etap> Etaps { get; set; }
    }
}
