﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Objects
{
    public class Dania
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
        public List<Etap> Etapy { get; set; }
    }
}
