﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Objects
{
    public class Etap
    {
        public int id { get; set; } // dla bazy danych
        public int nrCzynnosci { get; set; } //tabela 2.2 -  k
        public int liczbaPracownikow { get; set; } //tabela 2.2 -  p^{r}_{j,k}
        public int typPracownikow { get; set; }//tabela 2.2 -  tp{j,k}
        public int czasWykonania { get; set; }//tabela 2.2 -  t{j,k}
        public int nastepnikCzynnosci { get; set; }//tabela 2.2 -  pt{j,k}
        public int czasZwloki { get; set; }//tabela 2.2 -  l{j,k}
        public int stanowisko { get; set; }//tabela 2.2 -  st{j,k}
    }
}
