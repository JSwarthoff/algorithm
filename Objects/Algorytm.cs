﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Objects
{
    class Algorytm
    {
        public static int deadline { get; set; }
        public static int tempP { get; set; }
        public static int tempK { get; set; }
        public static double gamma { get; set; }
        public Algorytm(int [] param1)
        {
            deadline = param1[0];
            tempP = param1[1];
            tempK = param1[2];
            gamma = param1[3];
        }

        public List<Dania> wyzarzanie(List<Pracownik> pracLS, List<Stanowisko> stanLS, List<Dania> dania, MainWindow.Funkcja funkcjaHarmonogramu,MainWindow.Funkcja1 funkcjaEdycji)
        {
            int fWzor, fSzuk, fzmien;
            double P = 0, z, temp, delta, cos;
            Random rand = new Random();
            Dania[] wektorTempZmieniany = dania.ToArray();
            Dania[] wektorTemp = dania.ToArray();
            Dania[] wektorTempSzukany = dania.ToArray();
            temp = tempP;
            fWzor = funkcjaHarmonogramu(pracLS, stanLS, dania,false);
            fSzuk = funkcjaHarmonogramu(pracLS, stanLS, dania,false);

            while (temp >= tempK)
            {
                funkcjaEdycji(ref wektorTempZmieniany, rand);
                fzmien = funkcjaHarmonogramu(pracLS, stanLS, wektorTempZmieniany.ToList(),false);
                if (fzmien < fSzuk)
                {
                    for (int i = 0; i < wektorTemp.Length; i++)
                    {
                        wektorTempSzukany[i] = wektorTempZmieniany[i];
                    }
                    fSzuk = fzmien;
                }
                if (fzmien < fWzor)
                {
                    for (int i = 0; i < wektorTemp.Length; i++)
                    {
                        wektorTemp[i] = wektorTempZmieniany[i];
                    }
                    fWzor = fzmien;
                }
                else
                {
                    delta = fzmien - fWzor;
                    cos = Convert.ToDouble(-delta / temp);
                    P = Math.Exp(cos);
                    z = rand.NextDouble();
                    if (z <= P)
                    {
                        for (int i = 0; i < wektorTemp.Length; i++)
                        {
                            wektorTemp[i] = wektorTempZmieniany[i];
                        }
                        fWzor = fzmien;
                    }
                }
                temp = (0.0001*gamma) * temp;

            }

            funkcjaHarmonogramu(pracLS, stanLS, wektorTempSzukany.ToList(),true);
            return wektorTempSzukany.ToList();
        }

        public int alg1(List<Pracownik> pracLS, List<Stanowisko> stanLS, List<Dania> dania, bool show)
        {
            Etap[] ord = getOrd(dania);
            Etap etap = new Etap();
            int[,] tabSC = new int[ord.Length, 2];
            List<Pracownik> resPrac = new List<Pracownik>();
            List<Stanowisko> resStan = new List<Stanowisko>();
            int[,] A = new int[ord.Length, 4/*pracLS.Count + stanLS.Count*/];
            int S = 0, C = 0, help=0;
            for(int k = 0; k < ord.Length; k++)
            {
                resPrac.Clear();
                resStan.Clear();
                etap = ord[k];
                resPrac = pracLS.FindAll(p=>p.typ==ord[k].typPracownikow).ToList();
                resPrac.Sort((p,q )=> p.R.CompareTo(q.R));
                if (resPrac.Count > ord[k].liczbaPracownikow)
                {
                    for (int j = 0; j < ord[k].liczbaPracownikow; j++)
                    {
                        resPrac.RemoveAt(resPrac.Count - 1);
                    }
                }
                resStan = stanLS.FindAll(p => p.typ == ord[k].stanowisko).ToList();
                resStan.Sort((p, q) => p.R.CompareTo(q.R));
                help = resStan.Count - 1;
                for (int j = 0; j < help; j++)
                {
                    resStan.RemoveAt(resStan.Count - 1);
                }
                if (ord[k].nastepnikCzynnosci == 0)
                    S = 0;
                else
                {
                    S = tabSC[k - 1, 1];
                }
                S = GetMaxS(resPrac, resStan, S);
                C = S + ord[k].czasWykonania;
                for(int j = 0; j < resPrac.Count; j++)
                {
                    pracLS.Find(p => p.id == resPrac[j].id).R=C; 
                }
                for (int j = 0; j < resStan.Count; j++)
                {
                    stanLS.Find(p => p.id == resStan[j].id).R = C;
                }
                tabSC[k, 0] = S;
                tabSC[k, 1] = C;
                for(int j = 0; j < resStan.Count; j++)
                {
                    A[k, j] = resStan[j].id;
                }
                help = 0;
                for(int j = resStan.Count; j < resStan.Count + resPrac.Count; j++)
                {
                    A[k, j] = resPrac[help].id;
                    help++;
                }
            }
            int res = GetMaxS(pracLS, stanLS, 0) - GetMinC(pracLS, stanLS, 500, 500);
            if (show == true)
            {
                Window1 window1 = new Window1(pracLS.Count + stanLS.Count, tabSC, A);
                window1.Show();
            }
            return res;
        }

        public int alg2(List<Pracownik> pracLS, List<Stanowisko> stanLS, List<Dania> dania,bool show)
        {
            int D = deadline;
            Etap[] ord = getOrd(dania);
            Etap etap = new Etap();
            int[,]tabSC = new int[ord.Length, 2];
            List<Pracownik> resPrac = new List<Pracownik>();
            List<Stanowisko> resStan = new List<Stanowisko>();
            int[,]A = new int[ord.Length, 4];
            int S = D, C = D, help = 0;
            foreach(Pracownik i in pracLS)
            {
                i.R = D;
            }
            foreach(Stanowisko i in stanLS)
            {
                i.R = D;
            }
            for (int k = ord.Length-1; k >= 0; k--)
            {
                resPrac.Clear();
                resStan.Clear();
                etap = ord[k];
                resPrac = pracLS.FindAll(p => p.typ == ord[k].typPracownikow).ToList();
                resPrac.Sort((p, q) => p.R.CompareTo(q.R));
                if (resPrac.Count > ord[k].liczbaPracownikow)
                {
                    for (int j = 0; j < ord[k].liczbaPracownikow; j++)
                    {
                        resPrac.RemoveAt(0);
                    }
                }
                resStan = stanLS.FindAll(p => p.typ == ord[k].stanowisko).ToList();
                resStan.Sort((p, q) => p.R.CompareTo(q.R));
                help = resStan.Count - 1;
                for (int j = 0; j < help; j++)
                {
                    resStan.RemoveAt(0);
                }
                if (ord[k].nastepnikCzynnosci == 2)
                    C = D;
                else
                {
                    C = tabSC[k +1, 0];
                }
                C = GetMinC(resPrac, resStan, C,D);
                S = C - ord[k].czasWykonania;
                for (int j = 0; j < resPrac.Count; j++)
                {
                    pracLS.Find(p => p.id == resPrac[j].id).R = S;
                }
                for (int j = 0; j < resStan.Count; j++)
                {
                    stanLS.Find(p => p.id == resStan[j].id).R = S;
                }
                tabSC[k, 0] = S;
                tabSC[k, 1] = C;
                for (int j = 0; j < resStan.Count; j++)
                {
                    A[k, j] = resStan[j].id;
                }
                help = 0;
                for (int j = resStan.Count; j < resStan.Count + resPrac.Count; j++)
                {
                    A[k, j] = resPrac[help].id;
                    help++;
                }
            }
            if (show == true) 
            { 
                Window1 window1 = new Window1(pracLS.Count + stanLS.Count, tabSC, A);
                window1.Show();
            }
            int res = GetMaxS(pracLS, stanLS, 0) - GetMinC(pracLS, stanLS, 500, 500);
            return res;
        }
        public int alg3(List<Pracownik> pracLS, List<Stanowisko> stanLS, List<Dania> dania, bool show)
        {
            int D = deadline;
            Etap[] ord = getOrd(dania);
            Etap etap = new Etap();
            int[,] tabSC = new int[ord.Length, 2];
            List<Pracownik> resPrac = new List<Pracownik>();
            List<Stanowisko> resStan = new List<Stanowisko>();
            int[,] A = new int[ord.Length, 4];
            int S = D, C = D, help = 0, pt=0;
            foreach (Pracownik i in pracLS)
            {
                i.R = D;
            }
            foreach (Stanowisko i in stanLS)
            {
                i.R = D;
            }
            for (int k = ord.Length - 1; k >= 0; k--)
            {
                pt = 0;
                resPrac.Clear();
                resStan.Clear();
                etap = ord[k];
                resPrac = pracLS.FindAll(p => p.typ == ord[k].typPracownikow).ToList();
                resPrac.Sort((p, q) => p.R.CompareTo(q.R));
                if (resPrac.Count > ord[k].liczbaPracownikow)
                {
                    for (int j = 0; j < ord[k].liczbaPracownikow; j++)
                    {
                        resPrac.RemoveAt(0);
                    }
                }
                resStan = stanLS.FindAll(p => p.typ == ord[k].stanowisko).ToList();
                resStan.Sort((p, q) => p.R.CompareTo(q.R));
                help = resStan.Count - 1;
                for (int j = 0; j < help; j++)
                {
                    resStan.RemoveAt(0);
                }
                if (ord[k].nastepnikCzynnosci == 2)
                    C = D;
                else
                {
                    C = tabSC[k + 1, 0];
                }
                C = GetMinC(resPrac, resStan, C, D);
                S = C - ord[k].czasWykonania;
                for (int j = 0; j < resPrac.Count; j++)
                {
                    pracLS.Find(p => p.id == resPrac[j].id).R = S;
                }
                for (int j = 0; j < resStan.Count; j++)
                {
                    stanLS.Find(p => p.id == resStan[j].id).R = S;
                }
                tabSC[k, 0] = S;
                tabSC[k, 1] = C;
                for (int j = 0; j < resStan.Count; j++)
                {
                    A[k, j] = resStan[j].id;
                }
                help = 0;
                for (int j = resStan.Count; j < resStan.Count + resPrac.Count; j++)
                {
                    A[k, j] = resPrac[help].id;
                    help++;
                }
                while (ord[k+pt].nrCzynnosci != 3)
                {
                    if (tabSC[k + 1 + pt,0] - tabSC[k + pt,1] <= ord[k + 1 + pt].czasZwloki)
                    {
                        break;
                    }
                    tabSC[k + pt + 1,0] = tabSC[k + pt,1] + ord[k + pt + 1].czasZwloki;
                    tabSC[k + pt + 1,1] = tabSC[k + pt + 1,0] + ord[k + pt + 1].czasWykonania;
                    for(int i = 0; i < 4; i++)
                    {
                        if(pracLS.Find(p => p.id == A[k+pt+1, i]) != null)
                        {
                            pracLS.Find(p => p.id == A[k+pt+1, i]).R = tabSC[k + pt + 1, 0];
                        }
                        if(stanLS.Find(p => p.id == A[k+pt+1, i])!=null)
                        {
                            stanLS.Find(p => p.id == A[k+pt+1, i]).R = tabSC[k + pt + 1, 1];
                        }
                    }
                    pt++;
                    
                }
            }
            if (show == true)
            {
                Window1 window1 = new Window1(pracLS.Count + stanLS.Count, tabSC, A);
                window1.Show();
            }
            int res = GetMaxS(pracLS, stanLS, 0) - GetMinC(pracLS, stanLS, 500, 500);
            return res;
        }

        public void swap2(ref Dania[] wek, Random rand)
        {
            Dania temp;
            int a = rand.Next(0, wek.Length), b;
            b = rand.Next(0, wek.Length);
            temp = wek[a];
            wek[a] = wek[b];
            wek[b] = temp;
        }

        public void swap(ref Dania[] wek, Random rand)
        {
            Dania temp;
            int a = rand.Next(0, wek.Length - 1);
            temp = wek[a];
            wek[a] = wek[a + 1];
            wek[a + 1] = temp;
        }

        public void insert(ref Dania[] wek, Random rand)
        {
            Dania temp;
            int a = rand.Next(0, wek.Length), b;
            do
            {
                b = rand.Next(0, wek.Length);
            } while (a == b);
            if (a < b)
            {
                temp = wek[a];
                for (int i = a; i < b; i++)
                {
                    wek[i] = wek[i + 1];
                }
                wek[b] = temp;
            }
            else
            {
                temp = wek[a];
                for (int i = a; i > b; i--)
                {
                    wek[i] = wek[i - 1];
                }
                wek[b] = temp;
            }
        }

        public int GetMaxS(List<Pracownik> resP, List<Stanowisko> resS, int S)
        {
            int max = 0;
            foreach(Pracownik i in resP)
            {
                max = Math.Max(max, i.R);
            }
            foreach(Stanowisko i in resS)
            {
                max = Math.Max(max, i.R);
            }
            max = Math.Max(S, max);
            return max;
        }
        public int GetMinC(List<Pracownik> resP, List<Stanowisko> resS, int C,int D)
        {
            int min = D;
            foreach (Pracownik i in resP)
            {
                min = Math.Min(min, i.R);
            }
            foreach (Stanowisko i in resS)
            {
                min = Math.Min(min, i.R);
            }
            min = Math.Min(C, min);
            return min;
        }


        public Etap [] getOrd(List<Dania> dania)
        {
            List<Etap> etapdanias = new List<Etap>();
            int i = 0;
            while (i < dania.Count)
            {
                dania[i].Etapy.Sort((p, q) => p.nrCzynnosci.CompareTo(q.nrCzynnosci));
                foreach (Etap r in dania[i].Etapy)
                {
                    if (r.nrCzynnosci != 4)
                        etapdanias.Add(r);
                }
                i++;
            }
            Etap[] eta = new Etap[etapdanias.Count];
            for (int j = 0; j < etapdanias.Count; j++)
            {

                eta[j] = etapdanias[j];
            }
            return eta;
        }
    }
}
